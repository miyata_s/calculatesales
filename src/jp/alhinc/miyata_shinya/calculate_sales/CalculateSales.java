package jp.alhinc.miyata_shinya.calculate_sales;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class CalculateSales {

	public static void main(String[] args) {
		HashMap<String, String> stmap = new HashMap<String, String>(); // マップ①
		HashMap<String, Double> stsales = new HashMap<String, Double>(); // マップ②
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str) {
				return new File(file, str).isFile() && str.matches("^\\d{8}.rcd$");// 拡張子を選択&&ファイル名選択
			}
		};


		BufferedReader br = null;
		BufferedReader br2 = null;

		File file = new File(args[0], "branch.lst"); // 抽出リストを選択
		try {
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			if (!file.exists()) { // 何もしない
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			String line; // 1行ずつ読み出し開始
			while ((line = br.readLine()) != null){
				String[] stinfo = line.split(",",0); // コードと名前分割

				if (stinfo.length != 2 || !stinfo[0].matches("^\\d{3}$")){
					System.out.println("支店定義フォーマットが不正です");
					return;
				}
				stmap.put(stinfo[0],stinfo[1]); // (コード,売上額)
				stsales.put(stinfo[0], (double) 0); // (コード,売上額)
			}

			File[] files = new File(args[0]).listFiles(filter); // 指定したフォルダのファイルから
			Arrays.sort(files);
			for (int i = 0; i < files.length - 1; i++) {
				int numa = Integer.parseInt(files[i].getName().substring(1, 8));
				int numb = Integer.parseInt(files[i + 1].getName().substring(1, 8));
				if (numa + 1 != numb) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			// 売上ファイルの読み込み
			for (int i = 0; i < files.length; i++) {
				FileReader fr2 = new FileReader(files[i]); // 1行ずつ読み込み
				br2 = new BufferedReader(fr2);

				// 売上ファイルの書き込み
				String cdate;
				String sdate;
				String xdate;

				cdate = br2.readLine(); // 1行目
				sdate = br2.readLine(); // 2行目
				xdate = br2.readLine(); // 3行目(空白)
				fr2.close();

				if (xdate != null) {
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return;
				}

				if (stmap.containsKey(cdate)) {

				} else {
					System.out.println(files[i].getName() + "の支店コードが不正です");
					return;
				}

				// 各支店ごとに抽出＆合計
				if (stsales.containsKey(cdate)) {
					double value = stsales.get(cdate);
					double sdate2 = Double.parseDouble(sdate);
					stsales.put(cdate, value + sdate2);

					double d1 = 9999999999d;
					if(stsales.get(cdate) > d1){
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				}
			}

			File outPut = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter(outPut);
			BufferedWriter bw = new BufferedWriter(fw);

			for (String cdate2 : stmap.keySet()) {
				bw.write(cdate2 + "," + stmap.get(cdate2) + "," + stsales.get(cdate2));
				bw.newLine();
			}
				bw.close();
				br2.close();

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			System.out.println(e);
		} finally {
			System.out.println("処理を終了します");
			if (br != null) {
				try {
					br.close();

				} catch (IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
	}

}
